/* Copié-collé d'Isa*/
/*export function Compteur() {
    // Déclare une nouvelle variable d'état, que l'on va appeler « count »
    const [count, setCount] = useState(0);
  
    return (
      <div>
        <p>Vous avez cliqué {count} fois</p>
        <button onClick={() => setCount(count + 1)}>
          Cliquez ici
        </button>
      </div>
    );
  }*/





/*Fait par Kristen */
import React, { useState } from 'react';

export function Compteur () {
    const [nbClicks, setNbClicks] = useState(0);

    function handleClick() {
        console.log(nbClicks)
        setNbClicks(nbClicks + 1);
    }

    return (
        <div>
            <p>
                Vous avez cliqué {nbClicks} fois
            </p>
            <button onClick={handleClick}>
                Clickez ici
            </button>
        </div>
    )
}