import logo from './logo.svg';
import './App.css';
import {Carte} from './carte/carte';
import { Compteur } from './compteur/Compteur';
import golden2 from './images/golden2.jpg';



function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />

        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
        </a>
        <p>A vous de jouer !</p>
        <div count className='compteur'>
        </div>
        <div className='user'>
          <Carte value={1} color="blue" imgsrc={golden2}/>
          <Carte value={2} color="yellow"/>
          <Carte value={3} color="red"/>
          <Carte value={4} color="green"/>
          <Carte value={5} color="white"/>
          <Carte value={6} color="purple"/>
        </div>
        <Compteur/>
      </header>
    </div>
  );
}

export default App;
