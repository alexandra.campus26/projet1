import React, { useState } from 'react';
import './carte.css';

export function Carte(props) {
    const [switcheur, setSwitch] = useState("active");

    function handlerSwitch(e) {
        if (switcheur === "active") {
            setSwitch("inactive");
        }
        else {
            setSwitch("active");
        }
    }

    return (
        <div /*onClick={function() { alert('clic'); }}*/ className={`Carte ${switcheur}`}
            style={{ background: props.color }} onClick={handlerSwitch}>
            <div>
            <div class="recto">{props.value}</div>
                <div class="verso"></div>
            </div>
        </div>
    )
}
/*<div class="recto">{props.value}</div>*/ /*dans return, pour qu'1 chiffre s'affiche sur le recto, au lieu de l'image*/
/*<div class="recto" style={{backgroundImage:url(${props.imgsrc}) }}></div>*/ /*dans return, pour qu'1 image s'affiche sur le recto, au lieu du chiffre*/

